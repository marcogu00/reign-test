import {
    RECEIVE_POSTS,
    LOADING_POSTS,
    CHANGE_CURRENT_PAGE,
    CHANGE_POSTS_PER_PAGE,
    ERROR_GETTING_POSTS
} from "../actionTypes";

const initialState = {
    posts: [],
    currentPage: 0,
    postsPerPage: 20,
    total: 0,
    pages: 1,
    loading: false,
    error: null
};

export default function(state = initialState, action) {
    switch (action.type) {
        case RECEIVE_POSTS: {
            return {
                ...state,
                posts: action.payload.posts,
                total: action.payload.count,
                pages: action.payload.totalPages
            };
        }
        case LOADING_POSTS: {
            return {
                ...state,
                loading: action.payload
            };
        }
        case CHANGE_CURRENT_PAGE: {
            if (state.currentPage === action.payload) {
                return state;
            }

            return {
                ...state,
                currentPage: action.payload
            };
        }
        case CHANGE_POSTS_PER_PAGE: {
            if (state.postsPerPage === action.payload) {
                return state;
            }

            return {
                ...state,
                postsPerPage: action.payload
            };
        }

        case ERROR_GETTING_POSTS: {
            return {
                ...state,
                error: action.payload
            };
        }

        default:
            return state;
    }
}
