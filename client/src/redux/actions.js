import {
    RECEIVE_POSTS,
    LOADING_POSTS,
    CHANGE_CURRENT_PAGE,
    CHANGE_POSTS_PER_PAGE,
    ERROR_GETTING_POSTS
} from "./actionTypes";
import API from "../utils/api";

export const getPosts = content => {
    return (dispatch, getState) => {
        let data = {
            postsPerPage: getState().posts.postsPerPage,
            page: getState().posts.currentPage,
            ignored: JSON.parse(localStorage.getItem("ignored")) || []
        };

        dispatch(loadingPosts(true));

        API.post("/api", data)
            .then(response => {
                dispatch(receivePosts(response.data));
                dispatch(loadingPosts(false));
            })
            .catch(error => {
                dispatch(loadingPosts(false));
                dispatch(errorGettingPosts(error));
                console.log(error);
            });
    };
};

export const receivePosts = content => {
    return {
        type: RECEIVE_POSTS,
        payload: content
    };
};

export const loadingPosts = value => {
    return {
        type: LOADING_POSTS,
        payload: value
    };
};

export const changePostsPerPage = value => {
    return {
        type: CHANGE_POSTS_PER_PAGE,
        payload: value
    };
};

export const changeCurrentPage = value => {
    return {
        type: CHANGE_CURRENT_PAGE,
        payload: value
    };
};

export const errorGettingPosts = value => {
    return {
        type: ERROR_GETTING_POSTS,
        payload: value
    };
};
