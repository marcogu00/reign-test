import React from "react";
import IconButton from "@material-ui/core/IconButton";
import DeleteRoundedIcon from "@material-ui/icons/DeleteRounded";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { Transition } from "react-transition-group";
import { connect } from "react-redux";
import { getPosts } from "../redux/actions";

class SinglePost extends React.Component {
    constructor(props) {
        super(props);
        this.state = { show: true };
    }

    deletePost(e, id) {
        e.preventDefault();

        let deletedPosts = JSON.parse(localStorage.getItem("ignored"));

        this.setState({ show: false });

        if (deletedPosts != null) {
            if (deletedPosts.indexOf(id) === -1) {
                deletedPosts.push(id);
            }
        } else {
            deletedPosts = [id];
        }

        localStorage.setItem("ignored", JSON.stringify(deletedPosts));
    }

    formatDate(date) {
        let formattedDate = new Date(date);
        let today = new Date();

        // If it is today
        if (formattedDate.toDateString() == today.toDateString()) {
            return formattedDate.toLocaleString("en-US", {
                hour: "numeric",
                minute: "numeric",
                hour12: true
            });
        }

        // If it is yesterday
        if (formattedDate.getDate() == today.getDate() - 1) {
            return "Yesterday";
        }

        return formattedDate.toLocaleString("en-US", {
            month: "short",
            day: "numeric"
        });
    }

    render() {
        const defaultStyle = {
            opacity: 0,
            maxHeight: "400px",
            overflow: "hidden"
        };

        const transitionStyles = {
            entering: { opacity: 1, maxHeight: "400px" },
            entered: { opacity: 1, maxHeight: "400px" },
            exiting: { opacity: 0, maxHeight: "0" },
            exited: { opacity: 0, maxHeight: "0" }
        };

        const { post } = this.props;

        return (
            <Transition unmountOnExit={true} in={this.state.show} timeout={400}>
                {state => (
                    <a
                        style={{ textDecoration: "none" }}
                        href={post.story_url || post.url}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <Grid
                            container
                            item
                            alignItems="center"
                            spacing={4}
                            style={{
                                ...defaultStyle,
                                ...transitionStyles[state]
                            }}
                            className="post"
                        >
                            <Grid item xs={12} md={8}>
                                <Typography
                                    display="inline"
                                    style={{ marginRight: "10px" }}
                                >
                                    {post.story_title || post.title}
                                </Typography>

                                <Typography
                                    display="inline"
                                    color="textSecondary"
                                    className="post-author"
                                >
                                    - {post.author} -
                                </Typography>
                            </Grid>

                            <Grid item xs={12} md={3}>
                                <Typography display="inline">
                                    {this.formatDate(post.created_at)}
                                </Typography>
                            </Grid>

                            <Grid
                                item
                                xs={12}
                                md={1}
                                style={{ textAlign: "right" }}
                                className="delete-post"
                            >
                                <IconButton
                                    aria-label="delete"
                                    onClick={e => this.deletePost(e, post.objectID)}
                                >
                                    <DeleteRoundedIcon />
                                </IconButton>
                            </Grid>
                        </Grid>
                    </a>
                )}
            </Transition>
        );
    }
}

export default connect(null, { getPosts })(SinglePost);
