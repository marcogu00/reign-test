import React from "react";
import Grid from "@material-ui/core/Grid";
import Skeleton from "@material-ui/lab/Skeleton";

function PostsLoader() {
    return [1, 2, 3, 4].map(item => (
        <Grid
            container
            item
            alignItems="center"
            spacing={4}
            key={item}
            className="post"
        >
            <Grid item xs={12} md={8}>
                <Skeleton animation="wave" width="80%" variant="text" />
            </Grid>

            <Grid item xs={12} md={3}>
                <Skeleton animation="wave" variant="text" width="60%" />
            </Grid>

            <Grid item xs={12} md={1} style={{ textAlign: "right" }}>
                <Skeleton
                    animation="wave"
                    variant="circle"
                    style={{ marginLeft: "auto" }}
                    width={30}
                    height={30}
                />
            </Grid>
        </Grid>
    ));
}

export default PostsLoader;
