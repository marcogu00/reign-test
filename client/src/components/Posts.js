import React from "react";
import TablePagination from "@material-ui/core/TablePagination";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import { getPosts, changePostsPerPage, changeCurrentPage } from "../redux/actions";
import { animateScroll as scroll } from "react-scroll";

import SinglePost from "./SinglePost";
import PostsLoader from "./PostsLoader";

class Posts extends React.Component {
    constructor(props) {
        super(props);
        this.state = { intervalId: null };
    }

    componentDidMount() {
        this.props.getPosts();
    }

    handleChangePage(e) {
        scroll.scrollToTop();
        this.props.changeCurrentPage(e);
        this.props.getPosts();
    }

    handleChangeRowsPerPage(e) {
        scroll.scrollToTop();
        this.props.changeCurrentPage(0);
        this.props.changePostsPerPage(e);
        this.props.getPosts();
    }

    render() {
        const { loading } = this.props.posts;
        const { error } = this.props.posts;
        const { posts, postsPerPage, currentPage, total } = this.props.posts;

        console.log(error);

        if (error !== null) {
            return (
                <Paper className="posts-error">
                    <Typography
                        variant="h5"
                        color="error"
                        align="center"
                        className="post-author"
                    >
                        There was an error fetching the posts, please try again.
                    </Typography>
                </Paper>
            );
        }

        if (posts.length === 0 && !loading) {
            return (
                <Paper className="no-posts-found">
                    <Typography variant="h5" align="center" className="post-author">
                        Sorry no results found :(
                    </Typography>
                </Paper>
            );
        }

        return (
            <Paper className="posts-wrapper">
                <div style={{ padding: "0 16px" }}>
                    {loading === true && <PostsLoader />}

                    {!loading &&
                        posts.length > 0 &&
                        posts.map(post => (
                            <SinglePost key={post.objectID} post={post} />
                        ))}
                </div>

                <TablePagination
                    rowsPerPageOptions={[5, 10, 20, 25]}
                    component="div"
                    count={total}
                    rowsPerPage={postsPerPage}
                    page={currentPage}
                    onChangePage={(e, page) => this.handleChangePage(page)}
                    onChangeRowsPerPage={e =>
                        this.handleChangeRowsPerPage(e.target.value)
                    }
                />
            </Paper>
        );
    }
}

const mapStateToProps = state => ({ posts: state.posts });

export default connect(mapStateToProps, {
    getPosts,
    changeCurrentPage,
    changePostsPerPage
})(Posts);
