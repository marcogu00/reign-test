import React from "react";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";

import Posts from "./components/Posts";

import "./index.css";

const useStyles = makeStyles({
    root: {
        background: "#232323",
        color: "white",
        display: "flex",
        padding: "50px 0 60px 0"
    },
    tableContainer: {
        paddingTop: "60px",
        paddingBottom: "60px"
    }
});

function App() {
    const classes = useStyles();

    return (
        <div className="main-app">
            <header className={classes.root}>
                <Container maxWidth="lg">
                    <h1>HN Feed</h1>
                    <h3>{"We <3 Hacker News!"}</h3>
                </Container>
            </header>

            <Container maxWidth="lg" className={classes.tableContainer}>
                <Posts />
            </Container>
        </div>
    );
}

export default App;
