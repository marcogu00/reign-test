import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import store from "./redux/store";

import CssBaseline from "@material-ui/core/CssBaseline";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
    typography: {
        fontFamily: "Muli, sans-serif",
        fontWeightMedium: 600,
        body1: {
            fontSize: "13px"
        },
        body2: {
            fontSize: "13px",
            fontWeight: 600
        },
        h1: {
            fontWeight: 700
        },
        h2: {
            fontWeight: 700
        },
        h3: {
            fontWeight: 700
        },
        h5: {
            fontWeight: 700
        }
    },
    palette: {
        text: {
            secondary: "#999"
        }
    }
});

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <CssBaseline />

            <App />
        </MuiThemeProvider>
    </Provider>,
    document.getElementById("root")
);
