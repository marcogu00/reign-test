### Docker

By default, the Docker will expose port 80 fo the client and the port 5000 for the api , so change this within the Dockerfile if necessary.

```sh
cd reign-test
$ docker-compose up --build
```

### Populate the database

Once the docker container is ready, you can access http://localhost:5000/api/populate wich will populate the databasae with 700 records from the api. You can change the number of records by passing a paramter to the url like so: http://localhost:5000/api/populate?records=100. The limit of records is 1000.

### Take a look
Once you populate the database you can go to http://localhost and interact with the front-end app.