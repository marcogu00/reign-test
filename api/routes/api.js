const express = require("express");
const ApiService = require("../services/api");
const validationHandler = require("../utils/middlewares/validationHandler");
const { insertPostsSchema, queryPostsSchema } = require("../utils/schemas/api");

function api(app) {
    const router = express.Router();

    app.use("/api", router);

    const api = new ApiService();

    router.post(
        "/",
        validationHandler(queryPostsSchema, "body"),
        async function(req, res, next) {
            let page = req.body.page;
            let postsPerPage = req.body.postsPerPage;
            let ignored = req.body.ignored;

            try {
                let posts = await api.getPosts(ignored, page, postsPerPage);
                res.status(200).json(posts);
            } catch (error) {
                next(error);
            }
        }
    );

    router.get(
        "/populate",
        validationHandler(insertPostsSchema, "query"),
        async function(req, res, next) {
            let records = req.query.records;

            try {
                let posts = await api.updatePosts(records);

                if (posts.errors) {
                    res.status(202).json({
                        insertedPosts: posts.result,
                        message: `There were ${posts.errors} duplicated posts. The rest were succesfully saved.`
                    });
                } else {
                    res.status(200).json({
                        result: posts,
                        message: "Saved successfully."
                    });
                }
            } catch (error) {
                next(error);
            }
        }
    );
}

module.exports = api;
