const MongoLib = require("../lib/mongo");
const axios = require("axios");

class ApiService {
    constructor() {
        this.collection = "posts";
        this.mongoDB = new MongoLib();
    }

    async getPosts(ignored, page, perPage, sort, asc) {
        let options = {
            query: ignored ? { objectID: { $nin: ignored } } : "",
            page: page,
            perPage: perPage,
            sort: sort,
            asc: asc
        };

        const posts = await this.mongoDB.get(this.collection, options);

        return posts || [];
    }

    async updatePosts(records = 700) {
        let data = await axios.get(
            "https://hn.algolia.com/api/v1/search_by_date?query=nodejs&page=0&hitsPerPage=" +
                records
        );

        const postsIds = await this.mongoDB.insertMany(
            this.collection,
            data.data.hits
        );
        return postsIds || [];
    }
}

module.exports = ApiService;
