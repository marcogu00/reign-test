const { MongoClient, ObjectId } = require("mongodb");
const config = require("../config/index");

class MongoLib {
    constructor() {
        this.client = new MongoClient(config.mongoUrl + "/" + config.dbName, {
            useUnifiedTopology: true
        });
        this.dbName = config.dbName;
    }

    connect() {
        if (!MongoLib.connection) {
            MongoLib.connection = new Promise((resolve, reject) => {
                this.client.connect(err => {
                    if (err) {
                        reject(err);
                    }

                    resolve(this.client.db(this.dbName));
                });
            });
        }

        return MongoLib.connection;
    }

    getAll(collection, query) {
        this.connect().then(db => {
            return db
                .collection(collection)
                .find(query)
                .toArray();
        });
    }

    get(collection, options) {
        let query = options.query || {};
        let page = parseInt(options.page) || 0;
        let perPage = parseInt(options.perPage) || 20;
        let sortProp = options.sort || "created_at";
        let asc = options.asc ? 1 : -1;

        let sort = {};
        sort[sortProp] = asc;

        return this.connect().then(async db => {
            let data = await db
                .collection(collection)
                .find(query)
                .sort(sort)
                .skip(page * perPage)
                .limit(perPage);

            let posts = await data.toArray();
            let count = await data.count();

            return {
                posts: posts,
                count: count,
                totalPages: Math.ceil(count / perPage),
                page: page,
                perPage: perPage
            };
        });
    }

    create(collection, data) {
        return this.connect()
            .then(db => {
                return db.collection(collection).insertOne(data);
            })
            .then(result => result.insertedId);
    }

    insertMany(collection, data) {
        return this.connect()
            .then(db => {
                /*
                Creates the index from the objectID of the api
                if the index already exists this operation wont do anything
            */

                db.collection(collection).createIndex(
                    { objectID: 1 },
                    { unique: true }
                );

                return db
                    .collection(collection)
                    .insertMany(data, {
                        ordered: false
                    })
                    .then(result => ({
                        insertedIds: result.insertedIds,
                        insertedCount: result.insertedCount
                    }));
            })
            .catch(error => {
                if (error.code === 11000) {
                    return {
                        result: error.result.nInserted,
                        errors: error.writeErrors.length
                    };
                }

                return error;
            });
    }
}

module.exports = MongoLib;
