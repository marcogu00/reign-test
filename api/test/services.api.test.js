const assert = require("assert");
const nock = require("nock");
const proxyquire = require("proxyquire");
const { MongoMock, getStub, insertManyStub } = require("../utils/mocks/mongo");
const { postsMock, filteredIdsMock } = require("../utils/mocks/api");

describe("Services - Api", function() {
    const ApiService = proxyquire("../services/api", {
        "../lib/mongo": MongoMock
    });

    const apiService = new ApiService();

    describe("When getPosts method it called", async function() {
        it("should call Mongo get method", async function() {
            await apiService.getPosts();
            assert.strictEqual(getStub.called, true);
        });

        it("should return an array of posts", async function() {
            let result = await apiService.getPosts();
            assert.deepEqual(result.posts, postsMock);
        });
    });

    describe("When updatePosts method it called", async function() {
        // Mock axios request
        beforeEach(() => {
            nock("https://hn.algolia.com")
                .get(uri => uri.includes("api"))
                .reply(200, {
                    hits: postsMock
                });
        });

        it("should call Mongo insertMany method", async function() {
            await apiService.updatePosts();
            assert.strictEqual(insertManyStub.called, true);
        });

        it("should return an object with ids and count of inserted ids", async function() {
            let result = await apiService.updatePosts();
            assert.deepEqual(result, {
                insertedIds: filteredIdsMock,
                insertedCount: filteredIdsMock.length
            });
        });
    });
});
