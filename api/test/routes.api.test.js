const assert = require("assert");
const proxyquire = require("proxyquire");
const testServer = require("../utils/testServer");
const { ApiServiceMock, postsMock } = require("../utils/mocks/api");

describe("Routes - Api", function() {
    const route = proxyquire("../routes/api", {
        "../services/api": ApiServiceMock
    });

    const request = testServer(route);

    describe("GET Posts", function() {
        it("Should respond with status 200", function(done) {
            request
                .post("/api")
                .send({})
                .then(response => {
                    assert.equal(response.statusCode, 200);
                    done();
                })
                .catch(error => {
                    done(error);
                });
        });
    });

    describe("CREATE Posts", function() {
        it("Should respond with created posts Ids", function(done) {
            request
                .get("/api/populate")
                .then(response => {
                    let expectedResult = postsMock.map(m => m.objectID);
                    assert.deepEqual(response.body.result, expectedResult);
                    done();
                })
                .catch(error => {
                    done(error);
                });
        });
    });
});
