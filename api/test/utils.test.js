const assert = require("assert");
const routes = require("../routes/api");
const testServer = require("../utils/testServer");
const { ApiServiceMock, postsMock } = require("../utils/mocks/api");

describe("Utilities", function() {
    const request = testServer(routes);

    describe("When query a non available route", async function() {
        it("Should respond with status 404", async function() {
            let response = await request.get("/wrong");

            assert.equal(response.statusCode, 404);
        });
    });
});
