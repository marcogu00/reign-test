const cron = require("node-cron");
const ApiService = require("../services/api");

function cronJobs() {
    cron.schedule("0 0 */1 * * *", async function() {
        console.log("Fetching new posts");

        try {
            const api = new ApiService();

            let posts = await api.updatePosts(200);

            if (posts.errors) {
                console.log(
                    `There were ${posts.errors} duplicated posts. The other ${posts.result} were succesfully saved.`
                );
            } else {
                console.log(`Updated successfully.`);
            }
        } catch (error) {
            console.log("There was an error updating the posts");
        }
    });
}

module.exports = cronJobs;
