const sinon = require("sinon");

const { postsMock, filteredIdsMock } = require("./api");

const getStub = sinon.stub();
getStub.withArgs("posts").resolves({
    posts: postsMock,
    count: postsMock.length,
    totalPages: 1,
    page: 0,
    perPage: 20
});

const insertManyStub = sinon.stub();
insertManyStub.withArgs("posts").resolves({
    insertedIds: filteredIdsMock,
    insertedCount: filteredIdsMock.length
});

class MongoMock {
    get(collection, options) {
        return getStub(collection, options);
    }

    insertMany(collection, data) {
        return insertManyStub(collection, data);
    }
}

module.exports = {
    MongoMock,
    getStub,
    insertManyStub
};
