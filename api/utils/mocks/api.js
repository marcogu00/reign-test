const postsMock = [
    {
        created_at: "2020-03-16T11:34:46.000Z",
        title: null,
        url: null,
        author: "imtringued",
        points: null,
        story_text: null,
        comment_text:
            "It\u0026#x27;s all about batching. Sometimes doing a tiny change can cause the entire page to re-render. If you render your templates directly to the DOM you might start a rerendering chain that needlessly wastes performance. The other benefit is that the vDOM reduces the number of changes to a minimum. That\u0026#x27;s good but it\u0026#x27;s the rendering that kills performance. Changing 1000 or 10 DOM nodes doesn\u0026#x27;t matter that much.",
        num_comments: null,
        story_id: 22582145,
        story_title:
            "Asm-dom,a minimal WebAssembly virtual DOM to build C++ SPA",
        story_url: "https://mbasso.github.io/asm-dom/",
        parent_id: 22590983,
        created_at_i: 1584358486,
        _tags: ["comment", "author_imtringued", "story_22582145"],
        objectID: "22591802",
        _highlightResult: {
            author: {
                value: "imtringued",
                matchLevel: "none",
                matchedWords: []
            },
            comment_text: {
                value:
                    "It's all about batching. Sometimes doing a tiny change can cause the entire page to re-render. If you render your templates directly to the DOM you might start a rerendering chain that needlessly wastes performance. The other benefit is that the vDOM reduces the number of changes to a minimum. That's good but it's the rendering that kills performance. Changing 1000 or 10 DOM \u003cem\u003enodes\u003c/em\u003e doesn't matter that much.",
                matchLevel: "full",
                fullyHighlighted: false,
                matchedWords: ["nodejs"]
            },
            story_title: {
                value:
                    "Asm-dom, a minimal WebAssembly virtual DOM to build C++ SPA",
                matchLevel: "none",
                matchedWords: []
            },
            story_url: {
                value: "https://mbasso.github.io/asm-dom/",
                matchLevel: "none",
                matchedWords: []
            }
        }
    },
    {
        created_at: "2020-03-16T10:41:39.000Z",
        title: null,
        url: null,
        author: "highhedgehog",
        points: null,
        story_text: null,
        comment_text: "Ehy, fellow italian here. google",
        num_comments: null,
        story_id: 22575987,
        story_title:
            "Ask HN: I am in lockdown. Asking help to find remote work",
        story_url: null,
        parent_id: 22575987,
        created_at_i: 1584355299,
        _tags: ["comment", "author_highhedgehog", "story_22575987"],
        objectID: "22591538",
        _highlightResult: {
            author: {
                value: "highhedgehog",
                matchLevel: "none",
                matchedWords: []
            },
            comment_text: {
                value: "Ehy, at google",
                matchLevel: "full",
                fullyHighlighted: false,
                matchedWords: ["nodejs"]
            },
            story_title: {
                value:
                    "Ask HN: I am in lockdown. Asking help to find remote work",
                matchLevel: "none",
                matchedWords: []
            }
        }
    }
];

const filteredIdsMock = postsMock.map(p => p.objectID);

class ApiServiceMock {
    async getPosts() {
        return Promise.resolve(postsMock);
    }

    async updatePosts() {
        return Promise.resolve(filteredIdsMock);
    }
}

module.exports = {
    ApiServiceMock,
    filteredIdsMock,
    postsMock
};
