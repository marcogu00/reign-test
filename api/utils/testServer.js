const express = require("express");
const supertest = require("supertest");
const {
    errorHandler,
    logErrors,
    wrapErrors
} = require("./middlewares/errorHandlers");
const notFoundHandler = require("./middlewares/notFoundHandler");

function testServer(route) {
    const app = express();
    app.use(express.json());

    route(app);

    app.use(notFoundHandler);

    app.use(logErrors);
    app.use(wrapErrors);
    app.use(errorHandler);

    return supertest(app);
}

module.exports = testServer;
