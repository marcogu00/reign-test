const joi = require("@hapi/joi");

// const objectIdSchema = joi.object().keys({ userId: joi.string().regex(/^[0-9a-fA-F]{24}$/) });
const recordsQuantitySchema = joi
    .number()
    .integer()
    .less(1001);

const insertPostsSchema = joi.object().keys({
    records: recordsQuantitySchema
});

const queryPostsSchema = joi.object().keys({
    ignored: joi.array().items(joi.string()),
    postsPerPage: joi.number().integer(),
    page: joi.number().integer()
});

module.exports = {
    insertPostsSchema,
    queryPostsSchema
};
