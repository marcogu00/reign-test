require("dotenv").config();

module.exports = {
    dev: process.env.NODE_ENV.trim() !== "production",
    mongoUrl: process.env.MONGO_URL,
    dbName: process.env.DB_NAME,
    port: process.env.PORT
};
