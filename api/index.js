const express = require("express");
const app = express();
const cors = require("cors");
const config = require("./config/index");

const apiRoutes = require("./routes/api");

const {
    errorHandler,
    logErrors,
    wrapErrors
} = require("./utils/middlewares/errorHandlers");
const notFoundHandler = require("./utils/middlewares/notFoundHandler");
const cronJobs = require("./utils/crons");

app.use(express.json());

app.use(cors());

apiRoutes(app);

//Middleware para 404
app.use(notFoundHandler);

//Middlewares de errores

app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

cronJobs();

app.listen(config.port, () => {
    console.log("Listening on http://localhost:" + config.port);
});
